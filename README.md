# README #

Follow the instructions below for either the docker container approach, or the native approach if you do not have/want docker on your machine
The benefit of the docker container is that it has been set up to run this project without needing to edit your local install of php

### What is this repository for? ###

A report on the most common crime category in a given postcode, along with the monthly average of the crime (saved as a csv output) coded for a technical test. 

Further enhancement is possible to allow more dynamic processing, rather than a fixed input file, as well as a few extra tests

### Dependencies - Docker ###
* Composer
* Docker
* Docker-compose

### Dependencies - Native ###
* Composer
* PHP 7

### How do I get set up? ###

## Docker ##
1. Close this repository on to your local machine
2. Navigate to the base directory of the project
3. Install composer dependencies using the command [composer install]
4. Run the docker container with the command [docker-compose up]
5. In another tab, enter the container with the command [docker exec -ti crimer_application /bin/bash]
6. You will now be in the docker container, in the html folder. Navigate one level up with the command [cd ..]
7. Run the application with the command [php application.php]

## Native ##
1. Close this repository on to your local machine
2. Navigate to the base directory of the project
3. Install composer dependencies using the command [composer install]
7. Run the application with the command [php application.php]

Your report file will be found in the folder /src/Asset/Output with the name format of yyyymmdd-hhmmss.csv

## Unit Tests ##
A phpunit.xml file has been included to handle test locations. To run the unit tests, navigate to the base directory of the project and use the command [vendor/bin/phpunit/]