<?php

require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;
use OdeToIgnorance\CrimeReporter\Facade\CrimeReporter;
use OdeToIgnorance\CrimeReporter\Helper\CrimeFinderHelper;
use OdeToIgnorance\CrimeReporter\Helper\CrimeReportHelper;
use OdeToIgnorance\CrimeReporter\Helper\LatLongConverterHelper;

/* DI SETUP */
$client = new Client;
$latLongConverter = new LatLongConverterHelper($client);
$crimeFinder = new CrimeFinderHelper($client);
$reporter = new CrimeReportHelper();

/* run application */
$reporter = new CrimeReporter($latLongConverter, $crimeFinder, $reporter);
$reporter->generateReport();
