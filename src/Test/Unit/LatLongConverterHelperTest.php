<?php

namespace OdeToIgnorance\CrimeReporter\Test\Unit;

use GuzzleHttp\Client;
use OdeToIgnorance\CrimeReporter\Helper\LatLongConverterHelper;
use PHPUnit\Framework\TestCase;

class LatLongConverterHelperTest extends TestCase
{
    protected $converter;

    public function setUp()
    {
        $this->converter = new LatLongConverterHelper(new Client);
    }

    public function testConverterReturnsSuccessResponse()
    {
        $this->converter->convertFromPostCode('LE16RS');
        $this->assertEquals(200, $this->converter->getResponseStatus());
    }

    public function testConverterReturnsErrorResultFromBadPostCode()
    {
        $this->converter->convertFromPostCode('nope');
        $this->assertEquals(404, $this->converter->getResponseStatus());
    }

    public function testConverterReturnsErrorResultFromEmptyString()
    {
        $this->converter->convertFromPostCode('');
        $this->assertEquals(400, $this->converter->getResponseStatus());
    }

    public function testGetLatitudeFromPostCode()
    {
        $this->converter->convertFromPostCode('LE16RS');
        $this->assertEquals('52.62976', $this->converter->getLatitude());
    }

    public function testGetLongitudeFromPostCode()
    {
        $this->converter->convertFromPostCode('LE16RS');
        $this->assertEquals('-1.13185', $this->converter->getLongitude());
    }
}
