<?php

namespace OdeToIgnorance\CrimeReporter\Test\Unit;

use GuzzleHttp\Client;
use OdeToIgnorance\CrimeReporter\Helper\CrimeFinderHelper;
use PHPUnit\Framework\TestCase;

class CrimeFinderHelperTest extends TestCase
{
    protected $crimeFinder;

    public function setUp()
    {
        $this->crimeFinder = new CrimeFinderHelper(new Client);
    }

    public function testCrimeFinderReturnsResults()
    {

        $this->crimeFinder->findByLatitudeAndLongitudeAndDate('52.629756612174', '-1.1318540493336', '2016-01');
        $this->assertNotEmpty($this->crimeFinder->getCrimes());
    }

    public function testCrimeFinderResultsContainACategory()
    {
        $this->crimeFinder->findByLatitudeAndLongitudeAndDate('52.629756612174', '-1.1318540493336', '2016-01');
        $this->assertArrayHasKey('category', $this->crimeFinder->getCrimes()[0]);
    }
}
