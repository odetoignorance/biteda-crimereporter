<?php

namespace OdeToIgnorance\CrimeReporter\Facade;

use DateTime;
use OdeToIgnorance\CrimeReporter\Helper\CrimeFinderHelperInterface;
use OdeToIgnorance\CrimeReporter\Helper\CrimeReportHelper;
use OdeToIgnorance\CrimeReporter\Helper\LatLongConverterHelperInterface;

class CrimeReporter
{
    const ASSET_DIR = __DIR__.'/../Asset/';

    protected $latLongConverter;

    protected $crimeFinder;

    public function __construct(
        LatLongConverterHelperInterface $latLongConverter,
        CrimeFinderHelperInterface $crimeFinder,
        CrimeReportHelper $report
    ) {
        $this->latLongConverter = $latLongConverter;
        $this->crimeFinder = $crimeFinder;
        $this->report = $report;
    }

    public function generateReport()
    {
        $crimeData = $this->fetchCrimeData();
        $this->report->setData($crimeData);
        $report = $this->report->highestCrimeCategoryInYearWithAveragePerMonth();

        $fileName = $this->saveOutputFile($report);

        echo sprintf('Report saved in file %s', $fileName);
    }

    protected function fetchCrimeData() : array
    {
        $postCodes = $this->processInputFile(self::ASSET_DIR.'Input/postcodes.txt');
        $crimeData = [];

        foreach ($postCodes as $postCode) {
            for ($month = 1; $month <= 12; $month++) {
                $this->latLongConverter->convertFromPostCode($postCode);
                $this->crimeFinder->findByLatitudeAndLongitudeAndDate(
                    $this->latLongConverter->getLatitude(),
                    $this->latLongConverter->getLongitude(),
                    sprintf('2016-%d', $month)
                );
                $crimeData[$postCode][$month] =$this->crimeFinder->getCrimes();
            }
        }

        return $crimeData;
    }

    protected function processInputFile(string $file) : array
    {
        $postCodes = [];
        $handle = fopen($file, 'r');

        if ($handle == true) {
            while (false !== ($line = fgets($handle))) {
                $postCodes[] = rtrim($line);
            }

            fclose($handle);
        }

        return $postCodes;
    }

    protected function saveOutputFile(array $report) : string
    {
        $now = new DateTime;
        $fileName = sprintf('%s.csv', $now->format('Ymd-His'));
        $handle = fopen(self::ASSET_DIR.'Output/'.$fileName, 'w');

        foreach ($report as $postCodeReport) {
            fputcsv($handle, $postCodeReport);
        }

        fclose($handle);

        return $fileName;
    }
}
