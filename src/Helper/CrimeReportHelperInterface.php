<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

interface CrimeReportHelperInterface
{
    public function setData(array $data) : void;

    public function highestCrimeCategoryInYearWithAveragePerMonth() : array;
}
