<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

class CrimeReportHelper implements CrimeReportHelperInterface
{
    protected $data;

    public function setData(array $data) : void
    {
        $this->data = $data;
    }

    public function highestCrimeCategoryInYearWithAveragePerMonth() : array
    {
        $report = [];
        $data = $this->processCrimeData($this->data);

        foreach ($data as $postCode => $postCodeData) {
            $report[$postCode]['postCode'] = $postCode;
            $report[$postCode]['highestCrimeCategory'] = array_search(max($postCodeData), $postCodeData);
            $report[$postCode]['averageMonthlyIncidentsOfHighest'] = round((max($postCodeData) / 12), 2);
        }

        return $report;
    }

    protected function processCrimeData(array $crimeData) : array
    {
        $crimeReport = [];

        foreach ($crimeData as $postCode => $postCodeCrimes) {
            foreach ($postCodeCrimes as $month => $postCodeMonthCrimes) {
                if (!empty($postCodeMonthCrimes)) {
                    foreach ($postCodeMonthCrimes as $crime) {
                        $category = $crime['category'];

                        if (!isset($crimeReport[$postCode][$category])) {
                            $crimeReport[$postCode][$category] = 0;
                        }

                        $crimeReport[$postCode][$category]++;
                    }
                }
            }
        }

        return $crimeReport;
    }
}
