<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

class LatLongConverterHelper implements LatLongConverterHelperInterface
{
    const API_ENDPOINT = 'http://api.postcodes.io/postcodes/%s';
    const PRECISION = 5;

    protected $client;

    protected $response;

    protected $longitude;

    protected $latitude;

    protected $responseStatus;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function convertFromPostCode(string $postCode): bool
    {
        $this->response = $this->client->request(
            'GET',
            sprintf(self::API_ENDPOINT, $postCode),
            [
                'http_errors' => false
            ]
        );
        $this->responseStatus = (int)$this->response->getStatusCode();

        if ($this->responseStatus !== 200) {
            return false;
        }

        $responseBody = json_decode($this->response->getBody(), true);

        $this->latitude = round((float) $responseBody['result']['latitude'], self::PRECISION);
        $this->longitude = round((float) $responseBody['result']['longitude'], self::PRECISION);

        return true;
    }

    public function getResponseStatus(): int
    {
        return $this->responseStatus;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }
}
