<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

interface LatLongConverterHelperInterface
{
    public function convertFromPostCode(string $postCode) : bool;

    public function getResponseStatus() : int;

    public function getLatitude() : string;

    public function getLongitude() : string;
}
