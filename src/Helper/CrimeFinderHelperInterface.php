<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

interface CrimeFinderHelperInterface
{
    public function findByLatitudeAndLongitudeAndDate(string $latitude, string $longitude, string $date) : bool;

    public function getCrimes() : array;
}
