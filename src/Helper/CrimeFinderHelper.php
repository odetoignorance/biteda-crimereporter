<?php

namespace OdeToIgnorance\CrimeReporter\Helper;

use GuzzleHttp\ClientInterface;

class CrimeFinderHelper implements CrimeFinderHelperInterface
{
    const API_ENDPOINT = 'https://data.police.uk/api/crimes-at-location?date=%s&lat=%s&lng=%s';

    protected $client;

    protected $crimes;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function findByLatitudeAndLongitudeAndDate(string $latitude, string $longitude, string $date): bool
    {
        $response = $this->client->request(
            'GET',
            sprintf(self::API_ENDPOINT, $date, $latitude, $longitude),
            [
                'http_errors' => false
            ]
        );

        if ($response->getStatusCode() != 200) {
            return false;
        }

        $this->crimes = json_decode($response->getBody(), true);

        return true;
    }

    public function getCrimes(): array
    {
        return $this->crimes;
    }
}
